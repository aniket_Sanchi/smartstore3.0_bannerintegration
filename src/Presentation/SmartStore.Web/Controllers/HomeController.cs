﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using SmartStore.Core.Domain.Common;
using SmartStore.Core.Domain.Customers;
using SmartStore.Core.Domain.Messages;
using SmartStore.Core.Domain.Seo;
using SmartStore.Services;
using SmartStore.Services.Catalog;
using SmartStore.Services.Customers;
using SmartStore.Services.Localization;
using SmartStore.Services.Messages;
using SmartStore.Services.Search;
using SmartStore.Services.Seo;
using SmartStore.Services.Topics;
using SmartStore.Web.Framework.Controllers;
using SmartStore.Web.Framework.Security;
using SmartStore.Web.Framework.UI.Captcha;
using SmartStore.Web.Models.Common;
using SmartStore.Core.Infrastructure;
using SmartStore.Core.Domain.Cms;
using SmartStore.Core;
using SmartStore.Services.Media;
using System.Collections.Generic;

namespace SmartStore.Web.Controllers
{
    public partial class HomeController : PublicControllerBase
	{
		private readonly ICommonServices _services;
		private readonly Lazy<ICategoryService> _categoryService;
		private readonly Lazy<IProductService> _productService;
		private readonly Lazy<IManufacturerService> _manufacturerService;
		private readonly Lazy<ICatalogSearchService> _catalogSearchService;
		private readonly Lazy<CatalogHelper> _catalogHelper;
		private readonly Lazy<ITopicService> _topicService;
		private readonly Lazy<IQueuedEmailService> _queuedEmailService;
		private readonly Lazy<IEmailAccountService> _emailAccountService;
		private readonly Lazy<IXmlSitemapGenerator> _sitemapGenerator;
		private readonly Lazy<CaptchaSettings> _captchaSettings;
		private readonly Lazy<CommonSettings> _commonSettings;
		private readonly Lazy<SeoSettings> _seoSettings;
		private readonly Lazy<CustomerSettings> _customerSettings;

		public HomeController(
			ICommonServices services,
			Lazy<ICategoryService> categoryService,
			Lazy<IProductService> productService,
			Lazy<IManufacturerService> manufacturerService,
			Lazy<ICatalogSearchService> catalogSearchService,
			Lazy<CatalogHelper> catalogHelper,
			Lazy<ITopicService> topicService,
			Lazy<IQueuedEmailService> queuedEmailService,
			Lazy<IEmailAccountService> emailAccountService,
			Lazy<IXmlSitemapGenerator> sitemapGenerator,
			Lazy<CaptchaSettings> captchaSettings,
			Lazy<CommonSettings> commonSettings,
			Lazy<SeoSettings> seoSettings,
			Lazy<CustomerSettings> customerSettings)
        {
			this._services = services;
			this._categoryService = categoryService;
			this._productService = productService;
			this._manufacturerService = manufacturerService;
			this._catalogSearchService = catalogSearchService;
			this._catalogHelper = catalogHelper;
			this._topicService = topicService;
			this._queuedEmailService = queuedEmailService;
			this._emailAccountService = emailAccountService;
			this._sitemapGenerator = sitemapGenerator;
			this._captchaSettings = captchaSettings;
			this._commonSettings = commonSettings;
			this._seoSettings = seoSettings;
            this._customerSettings = customerSettings;
        }


        [RequireHttpsByConfigAttribute(SslRequirement.No)]
        public ActionResult Index()
        {
			return View();
        }

		public ActionResult StoreClosed()
		{
			return View();
		}

		[RequireHttpsByConfigAttribute(SslRequirement.No)]
		public ActionResult ContactUs()
		{
            var topic = _topicService.Value.GetTopicBySystemName("ContactUs", _services.StoreContext.CurrentStore.Id);

            var model = new ContactUsModel()
			{
				Email = _services.WorkContext.CurrentCustomer.Email,
				FullName = _services.WorkContext.CurrentCustomer.GetFullName(),
				DisplayCaptcha = _captchaSettings.Value.Enabled && _captchaSettings.Value.ShowOnContactUsPage,
                DisplayPrivacyAgreement = _customerSettings.Value.DisplayPrivacyAgreementOnContactUs,
                MetaKeywords = topic.GetLocalized(x => x.MetaKeywords),
                MetaDescription = topic.GetLocalized(x => x.MetaDescription),
                MetaTitle = topic.GetLocalized(x => x.MetaTitle),
            };

			return View(model);
		}

		[HttpPost, ActionName("ContactUs")]
		[CaptchaValidator]
		public ActionResult ContactUsSend(ContactUsModel model, bool captchaValid)
		{
			//validate CAPTCHA
			if (_captchaSettings.Value.Enabled && _captchaSettings.Value.ShowOnContactUsPage && !captchaValid)
			{
				ModelState.AddModelError("", T("Common.WrongCaptcha"));
			}

			if (ModelState.IsValid)
			{
				string email = model.Email.Trim();
				string fullName = model.FullName;
				string subject = T("ContactUs.EmailSubject", _services.StoreContext.CurrentStore.Name);

				var emailAccount = _emailAccountService.Value.GetDefaultEmailAccount();

				string from = null;
				string fromName = null;
				string body = Core.Html.HtmlUtils.FormatText(model.Enquiry, false, true, false, false, false, false);
				//required for some SMTP servers
				if (_commonSettings.Value.UseSystemEmailForContactUsForm)
				{
					from = emailAccount.Email;
					fromName = emailAccount.DisplayName;
					body = string.Format("<strong>From</strong>: {0} - {1}<br /><br />{2}",
						Server.HtmlEncode(fullName),
						Server.HtmlEncode(email), body);
				}
				else
				{
					from = email;
					fromName = fullName;
				}
				_queuedEmailService.Value.InsertQueuedEmail(new QueuedEmail
				{
					From = from,
					FromName = fromName,
					To = emailAccount.Email,
					ToName = emailAccount.DisplayName,
					Priority = 5,
					Subject = subject,
					Body = body,
					CreatedOnUtc = DateTime.UtcNow,
					EmailAccountId = emailAccount.Id,
					ReplyTo = email,
					ReplyToName = fullName
				});

				model.SuccessfullySent = true;
				model.Result = T("ContactUs.YourEnquiryHasBeenSent");

				//activity log
				_services.CustomerActivity.InsertActivity("PublicStore.ContactUs", T("ActivityLog.PublicStore.ContactUs"));

				return View(model);
			}

			model.DisplayCaptcha = _captchaSettings.Value.Enabled && _captchaSettings.Value.ShowOnContactUsPage;
			return View(model);
		}

		[RequireHttpsByConfigAttribute(SslRequirement.No)]
		public ActionResult SitemapSeo(int? index = null)
		{
			if (!_seoSettings.Value.XmlSitemapEnabled)
				return HttpNotFound();
			
			string content = _sitemapGenerator.Value.GetSitemap(index);

			if (content == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Sitemap index is out of range.");
			}

			return Content(content, "text/xml", Encoding.UTF8);
		}

		[RequireHttpsByConfigAttribute(SslRequirement.No)]
		public ActionResult Sitemap()
		{
            return RedirectPermanent(_services.StoreContext.CurrentStore.Url);
		}

        private string CheckButtonUrl(string url)
        {
            if (!String.IsNullOrEmpty(url))
            {
                if (url.StartsWith("//") || url.StartsWith("/") || url.StartsWith("http://") || url.StartsWith("https://"))
                {
                    //  //www.domain.de/dir
                    //  http://www.domain.de/dir
                    // nothing needs to be done
                    return url;
                }
                else if (url.StartsWith("~/"))
                {
                    //  ~/directory
                    return Url.Content(url);
                }
                else
                {
                    //  directory
                    return Url.Content("~/" + url);
                }
            }

            return url.EmptyNull();
        }

        [ChildActionOnly]
        public ActionResult ContentSlider()
        {
            var workContext = EngineContext.Current.Resolve<IWorkContext>();
            var storeContext = EngineContext.Current.Resolve<IStoreContext>();
            var pictureService = EngineContext.Current.Resolve<IPictureService>();
            var model = EngineContext.Current.Resolve<ContentSliderSettings>();

            model.BackgroundPictureUrl = pictureService.GetPictureUrl(model.BackgroundPictureId, 0, false);

            var slides = model.Slides
                .Where(s =>
                    s.LanguageCulture == workContext.WorkingLanguage.LanguageCulture &&
                    (!s.LimitedToStores || (s.SelectedStoreIds != null && s.SelectedStoreIds.Contains(storeContext.CurrentStore.Id)))
                )
                .OrderBy(s => s.DisplayOrder);

            foreach (ContentSliderSlideSettings slide in slides)
            {
                slide.PictureUrl = pictureService.GetPictureUrl(slide.PictureId, 0, false);
                //slide.Button1.Url = CheckButtonUrl(slide.Button1.Url);
                //slide.Button2.Url = CheckButtonUrl(slide.Button2.Url);
                //slide.Button3.Url = CheckButtonUrl(slide.Button3.Url);
            }
            //model.ContentSlides = new List<string> { "<div id='carousel-item-7' class='carousel-item bright product-slide active' style='background-color:#fdfdfd;height:500px'><div class='container slide-type-2'><div class='row h-100'><div class='col-md-6 text-md-right d-flex align-items-center text-center'><figure class='picture animated zoomIn' data-animation='animated zoomIn'><a href='http://demo.smartstore.com/frontend/en/charles-eames-lounge-chair-1956-2' title='Show details for Charles Eames Lounge Chair (1956)'><img alt='Picture of Charles Eames Lounge Chair (1956)' src='http://demo.smartstore.com/frontend/Media/Default/Thumbs/0000/0000355.png' title='Show details for Charles Eames Lounge Chair (1956)' class='img-fluid'></a></figure></div><div class='col-md-6 d-flex align-items-center'><div class='data text-md-left text-center'><h4 class='slide-title display-4 animated bounceInRight' data-animation='animated bounceInRight'><a href='http://demo.smartstore.com/frontend/en/charles-eames-lounge-chair-1956-2'><span>Charles Eames Lounge Chair(1956)</span></a></h4><p class='slide-subtitle fs-h4 d-none d-md-block animated bounceInRight' data-animation='animated bounceInRight'>Lounge chair, Designer Charles Eames, width 80 cm, depth 80 cm, height 60 cm, seat shell: plywood, foot(rotatable): cast aluminum, cushions (upholstered)with leather cover</p><div class='prices animated bounceInRight' data-animation='animated bounceInRight'><p class='product-price h2 animated tada' data-animation='animated tada'><span class='price'>From 1.619,10 €</span></p></div><div class='product-slide-buttons d-none d-md-block pt-3 animated bounceInUp' data-animation='animated bounceInUp'><a href='http://demo.smartstore.com/frontend/en/charles-eames-lounge-chair-1956-2' class='btn btn-clear-dark btn-animate btn-animate-right btn-lg' title='Description'><span><i class='fa fa-angle-right'></i>More information</span></a></div></div></div></div></div></div>",
            //    "<div id='carousel-item-25' class='carousel-item bright html-slide' style='background-image:url(http://demo.smartstore.com/frontend/Media/Default/Thumbs/0003/0003164.jpg);background-color: transparent;height:500px'><div class='container'><div class='row h-100'><div class='col-md-3 hidden-sm-down animated bounceInUp' style='animation-delay:1s'><a href = 'http://demo.smartstore.com/frontend/couturier-lady-edelstahl-grau' ><img alt='COUTURIER LADY Edelstahl-Grau' src='http://demo.smartstore.com/frontend/Media/Default/Uploaded/T035.210.11.011.00.png' style='width: 414px; height: 627px;' title='COUTURIER LADY Edelstahl-Grau'></a></div><div class='col-md-3 col-12 col-sm-6 animated bounceInLeft' style='animation-delay:1.5s'><a href = 'http://demo.smartstore.com/frontend/couturier-lady' ><img alt='COUTURIER LADY Alligator-Rot' class='d-none d-sm-inline-block' src='http://demo.smartstore.com/frontend/Media/Default/Uploaded/aligator_red_T035.210.16.011.01_h670.png' style='width: 414px; height: 670px;' title='COUTURIER LADY Alligator-Rot'></a></div><div class='col-md-3 col-sm-6 animated bounceInRight' style='animation-delay:1.5s'><a href = 'http://demo.smartstore.com/frontend/couturier-lady-leder-burgundrot' ><img alt='COUTURIER LADY Leder-Burgundrot' src='http://demo.smartstore.com/frontend/Media/Default/Uploaded/leder_burgundrot_T035.210.16.371.00.png' style='width: 414px; height: 627px;' title='COUTURIER LADY Leder-Burgundrot'></a></div><div class='col-md-3 d-none d-md-block animated bounceInUp' style='animation-delay:1s'><a href = 'http://demo.smartstore.com/frontend/couturier-lady-leder-schwarz' ><img alt='COUTURIER LADY Leder-Schwarz' src='http://demo.smartstore.com/frontend/Media/Default/Uploaded/T035.210.16.051.00.png' style='width: 414px; height: 627px;' title='COUTURIER LADY Leder-Schwarz'></a></div></div></div></div>"};

            model.Slides = slides.ToList();

            return PartialView(model);
        }
    }
}
