﻿using SmartStore.Core.Configuration;

namespace SmartStore.Core.Domain.Common
{
    public class CommonSettings : ISettings
    {
		public CommonSettings()
		{
			UseStoredProceduresIfSupported = true;
			AutoUpdateEnabled = true;
			EntityPickerPageSize = 48;
		}
		
		public bool UseSystemEmailForContactUsForm { get; set; }

        public bool UseStoredProceduresIfSupported { get; set; }

        public bool HideAdvertisementsOnAdminArea { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to display a warning if java-script is disabled
        /// </summary>
        public bool DisplayJavaScriptDisabledWarning { get; set; }

		public bool AutoUpdateEnabled { get; set; }

        /// <summary>
        /// Gets a sets a value indicating whether to full-text search is supported
        /// </summary>
        public bool UseFullTextSearch { get; set; }

        /// <summary>
        /// Gets or sets the page size for the entity picker
        /// </summary>
        public int EntityPickerPageSize { get; set; }

        /// <summary>
        /// Gets a sets a Full-Text search mode
        /// </summary>
        public FulltextSearchMode FullTextMode { get; set; }
    }
}